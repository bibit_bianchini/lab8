Pseudocode Lab 8 SPI Communications

Public functions


Initialize SPI (void)
No return value
  Activates clock to GPIO PortA
  Enables clock to SSI module 0
  Waits for GPIO port to be ready
  Programs GPIO for alternate functions
  PA2=SSIOClk, PA3=SSIOFss, PA4=SSIORx, PA5 SSIOTx
  Writes in GPIOCTL to select desired functions
  Programs PA2 to PA5 for digital I/O
  Programs PA2 to PA4 data directions
  Programs Pull-up for the clock line for SPI mode 3
  Waits for SSI0 to be ready
  Disables SSI0 0
  Select Master mode, and TXRIS for End of Transmission
  Configures SSI clock source to be the system clock
  Configures clock prescaler (Max clock rate 961k)
  Configure SCR, phase and polarity (Mode 3), mode (FRF), data size
  Locally enable interrupts (TXIM in SSIIM)
  Enable SSI
  Globally enable interrupts
  Enable NVIC bit 7 in EN0
  
  
TransmitSPI (uint8_t SendData)
Receives command value uint8_t
No return
  Writes SendData to SSI Data register
  Activates local interrupt
  
ReadSPI (void)
Returns uint8_t value
  Returns LastSPICommand
  
SPIEOTISR (void)
No return value, Interrupt Service Routine
  Locally disable interrupts
  Reads SPI data from register and stores in module level variable LastSPICommand
  Send Event to Control of Value received
  



  