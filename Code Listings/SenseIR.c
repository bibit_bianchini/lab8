/****************************************************************************
 Module
   SenseIR.c

 Revision
   1.0.1

 Description
   This is an IR sensing input capture interrupt module for ME218B Lab 8 for
   Team 3.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 02/04/2019     bibit   created file
****************************************************************************/

/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "PWM_Module.h"
#include "SenseIR.h"
#include "MotorFSM.h"

#include "ES_Types.h"
#include "termio.h"
#include "BITDEFS.H"
#include <stdint.h>
#include <stdbool.h>

#include "inc/hw_pwm.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_nvic.h"
#include "inc/hw_Timer.h"

#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/pwm.h"

/*----------------------------- Module Defines ----------------------------*/

// beacon definitions
#define TicksPerMs 40000
#define BeaconFrequency 1450
#define MeasurementError 20   // increase this if the IR signal isn't very
                              // clean or consistent. Value of 10 = 1% error

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file

static uint32_t LastCapture;
static uint32_t Period;
static bool     CanCalculatePeriod = false;

/*------------------------------ Module Code ------------------------------*/
//**************************************************************************/

/****************************************************************************
 Function
     InitializeSenseIR

 Parameters
     none

 Returns
     none

 Description
     Initializes input capture timer for measuring IR period. Uses Port C Pin
     4.

 Notes
     Gets called by CommandFSM initialization.

 Author
     Bibit Bianchini, 2/05/2019
****************************************************************************/
void InitializeSenseIR(void)
{
  // enable the clock to the timer (Wide Timer 0)
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R0;

  // enable the clock to Port C
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R2;

  // wait for the peripheral to be ready (Wide Timer 0)
  while ((HWREG(SYSCTL_PRWTIMER) & SYSCTL_RCGCWTIMER_R0) != SYSCTL_PRWTIMER_R0)
  {}

  // wait for the peripheral to be ready (Port C)
  while ((HWREG(SYSCTL_PRGPIO) & BIT2HI) != BIT2HI)
  {}

  // disable timer A before configuring
  HWREG(WTIMER0_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TAEN;

  // set up timer in 32-bit wide individual (not concatenated) mode
  // (don't get confused by the name of this constant -- 16 means individual)
  HWREG(WTIMER0_BASE + TIMER_O_CFG) = TIMER_CFG_16_BIT;

  // initialize the Interval Load Register to 0xffff.ffff
  HWREG(WTIMER0_BASE + TIMER_O_TAILR) = 0xffffffff;

  // set up timer A in capture mode (TAMR=3, TAAMS=0), for edge time
  // (TACMR=1), and for up-counting (TACDIR=1)
  HWREG(WTIMER0_BASE + TIMER_O_TAMR) =
      (HWREG(WTIMER0_BASE + TIMER_O_TAMR) & ~TIMER_TAMR_TAAMS) |
      (TIMER_TAMR_TACDIR | TIMER_TAMR_TACMR | TIMER_TAMR_TAMR_CAP);

  // set the event to rises only
  HWREG(WTIMER0_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TAEVENT_M;

  // set the alternate function for Port C bit 4 (WT0CCP0)
  HWREG(GPIO_PORTC_BASE + GPIO_O_AFSEL) |= BIT4HI;

  // map PC4's alternate function to WT0CCP0
  // (7 is the mux value, put into into nibble 4)
  HWREG(GPIO_PORTC_BASE + GPIO_O_PCTL) =
      (HWREG(GPIO_PORTC_BASE + GPIO_O_PCTL) & 0xfff0ffff) + (7 << 16);

  // enable PC4 for digital I/O
  HWREG(GPIO_PORTC_BASE + GPIO_O_DEN) |= BIT4HI;

  // make PC4 an input
  HWREG(GPIO_PORTC_BASE + GPIO_O_DIR) &= BIT4LO;

  // enable a local capture interrupt for the timer
  HWREG(WTIMER0_BASE + TIMER_O_IMR) |= TIMER_IMR_CAEIM;

  // enable timer A in Wide Timer 0 interrupt in the NVIC
  // it is interrupt number 94, so appears in EN2 at bit 30
  //HWREG(NVIC_EN2) |= BIT30HI; // commented out because MotorFSM will
  // determine when we want these interrupts to
  // occur.

  // make sure interrupts are enabled globally
  //__enable_irq(); // commented out because should be enabled in CommandFSM

  // enable the input capture timer
  // enable it to stall while stopped by the debugger
  HWREG(WTIMER0_BASE + TIMER_O_CTL) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL);
}

/****************************************************************************
 Function
     SenseIRISR

 Parameters
     none

 Returns
     none

 Description
     Interrupt response routine for IR detection.  When proper pulse is
     detected, this ISR disables its own interrupt, tells itself to reset
     period calculations for next time, and posts ALIGNED_WITH_BEACON event
     to MotorFSM module.

 Notes

 Author
     Bibit Bianchini, 2/05/2019
****************************************************************************/
void SenseIRISR(void)
{
  uint32_t ThisCapture;

  // start by clearing the source of the interrupt
  HWREG(WTIMER0_BASE + TIMER_O_ICR) = TIMER_ICR_CAECINT;

  // grab the captured value
  ThisCapture = HWREG(WTIMER0_BASE + TIMER_O_TAR);

  // check to see if we can capture the pulse yet
  if (CanCalculatePeriod == false)
  {
    // we can't calculate the period now, but we can next time
    CanCalculatePeriod = true;
  }
  else
  {
    // calculate the period
    Period = ThisCapture - LastCapture;

    // see if this period is within that of the known beacon signal
    // (if it matches exactly, PeriodInMS * Frequency should = 1000)
    if (((Period * BeaconFrequency) / TicksPerMs <= (1000 + MeasurementError))
        & ((Period * BeaconFrequency) / TicksPerMs >= (1000 - MeasurementError)))
    {
      // we found the beacon!
      // --> disable interrupts
      HWREG(NVIC_EN2) &= BIT30LO;

      // --> reset CanCalculatePeriod
      CanCalculatePeriod = false;

      // --> send ALIGNED_WITH_BEACON event to MotorFSM module
      ES_Event_t PostEvent;
      PostEvent.EventType = ALIGNED_WITH_BEACON;
      PostMotorFSM(PostEvent);
    }
  }

  // update LastCapture to prepare for the next edge
  LastCapture = ThisCapture;
}
