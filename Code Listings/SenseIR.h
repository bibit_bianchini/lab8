/****************************************************************************

  Header file for IR Sensing module for ME218B Lab 8 Team 3

 ****************************************************************************/

#ifndef SenseIR_H
#define SenseIR_H

#include "ES_Framework.h"

// Public Function Prototypes

void InitializeSenseIR(void);
void SenseIRISR(void);

#endif /* SenseIR_H */
