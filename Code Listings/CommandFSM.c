/****************************************************************************
 Module
   CommandFSM.c

 Revision
   1.0.1

 Description
   This is the highest level module for ME 218B Lab 8.  Implements a flat
   state machine under the Gen2 Events and Services Framework.

 Notes
   Team 3

 History
 When           Who     What/Why
 -------------- ---     --------
 02/04/2019     bibit   converted from templateFSM to lab 8 module
 01/15/12 11:12 jec     revisions for Gen2 framework
 11/07/11 11:26 jec     made the queue static
 10/30/11 17:59 jec     fixed references to CurrentEvent in RunTemplateSM()
 10/23/11 18:20 jec     began conversion from SMTemplate.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "CommandFSM.h"
#include "SenseIR.h"
#include "MotorFSM.h"
#include "SPIService.h"

#include "inc/hw_pwm.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_nvic.h"
#include "inc/hw_ssi.h"
#include "inc/hw_Timer.h"

/*----------------------------- Module Defines ----------------------------*/
#define AskCommandGeneratorTimer 0
#define AskCGTime 200  // check with command generator at 2ms 

#define QueryCGByte 0xAA

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match that of enum in header file
static CommandState_t CurrentState;
static uint8_t        LastCommand = 0xFF;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t  MyPriority;

//Module variables used for debugging
//static uint8_t  Rotation[] = { 0x03, 0x05, 0x02, 0x04 };
//static uint8_t  Drive[] = { 0x09, 0x11, 0x08, 0x10 };
//static uint8_t  i = 0;
/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitializeCommandFSM

 Parameters
     uint8_t : the priority of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine

 Notes

 Author
     Bibit Bianchini, 2/04/2019
****************************************************************************/
bool InitializeCommandFSM(uint8_t Priority)
{
  ES_Event_t ThisEvent;
  MyPriority = Priority;

  // go into InitializeCommand state
  CurrentState = InitializeCommand;

  // all port initializations
  // --> set up ports A and B by enabling the peripheral clocks
  HWREG(SYSCTL_RCGCGPIO) |= (BIT0HI | BIT1HI);
  // --> wait for the peripheral (A) to be ready
  while ((HWREG(SYSCTL_PRGPIO) & BIT0HI) != BIT0HI)
  {}
  // --> wait for the peripheral (B) to be ready
  while ((HWREG(SYSCTL_PRGPIO) & BIT1HI) != BIT1HI)
  {}

  // initialize input capture
  InitializeSenseIR();

  // initialize SPI
  InitializeSPIService();

  // enable interrupts globally (make sure not done in SPI or IR inits)
  __enable_irq();

  // set LastCommand = 0xff
  LastCommand = 0xff;

  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostCommandFSM

 Parameters
     ES_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue

 Notes

 Author
     Bibit Bianchini, 2/04/2019
****************************************************************************/
bool PostCommandFSM(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunCommandFSM

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   Communicates using SPI with command generator then sends commands to other
   modules when valid command received.

 Notes
   uses nested switch/case to implement the machine.

 Author
   Bibit Bianchini, 2/04/2019
****************************************************************************/
ES_Event_t RunCommandFSM(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (CurrentState)
  {
    case InitializeCommand:        // If current state is InitializeCommand
    {
      if (ThisEvent.EventType == ES_INIT)    // only respond to ES_Init
      {
        // start the timer to ask the command generator to send commands
        ES_Timer_InitTimer(AskCommandGeneratorTimer, AskCGTime);

        // now put the machine into the Check4Command state
        CurrentState = Check4Command;
      }
    }
    break;

    case Check4Command:        // If current state is state one
    {
      // if the AskCG timer timed out
      switch (ThisEvent.EventType)
      {
        case ES_TIMEOUT:
        {
          if (ThisEvent.EventParam == AskCommandGeneratorTimer)
          {
            // query the command generator
            TransmitSPI(QueryCGByte);
          }
        }
        break;

        case SPI_RECEIVED:
        {
          // based on the resulting command result:

          // if we received the same command as last time
          if (ThisEvent.EventParam == LastCommand)
          {
            // start AskCommandGenerator timer (0)
            ES_Timer_InitTimer(AskCommandGeneratorTimer, AskCGTime);
          }
          // if we received a new event that is 0xff, then next command will
          // be a new one
          else if (ThisEvent.EventParam == 0xff)
          {
            // set LastCommand = 0xff
            LastCommand = ThisEvent.EventParam;

            // start AskCommandGenerator timer (0)
            ES_Timer_InitTimer(AskCommandGeneratorTimer, AskCGTime);
          }
          // otherwise, we have a new command that is not 0xff, so it is a
          // valid command for our robot to follow
          else
          {
            // post a MOTOR_COMMAND event with EventParam of command byte
            // to the MotorService
            ES_Event_t PostEvent;
            PostEvent.EventType = MOTOR_COMMAND;
            PostEvent.EventParam = ThisEvent.EventParam;
            //Debugging aid for rotation
            //PostEvent.EventParam =Rotation[i%4];
            //Debugging aid for drive mode
            //PostEvent.EventParam =Drive[i%4];
            //Debugging aid for IR detect mode
            //PostEvent.EventParam=0x20;
            //i++;
            PostMotorFSM(PostEvent);

            // go into RunningCommand state
            CurrentState = RunningCommand;
          }
        }
        break;
      }
    }
    break;

    case RunningCommand:        // If current state is RunningCommand
    {
      // if Event was MOTOR_READY:
      if (ThisEvent.EventType == MOTOR_READY)
      {
        // start AskCommandGenerator timer (0)
        ES_Timer_InitTimer(AskCommandGeneratorTimer, AskCGTime);

        // go into the Check4Command state
        CurrentState = Check4Command;
      }
    }
    break;
  }                                   // end switch on Current State
  return ReturnEvent;
}
