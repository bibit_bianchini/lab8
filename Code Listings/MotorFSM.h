/****************************************************************************

  Header file for Motor Flat Sate Machine for ME 218B Lab 8 for
  Team 3, based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef MotorFSM_H
#define MotorFSM_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Framework.h"
#include "PWM_Module.h"

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  InitializeMotor, Idle, DrivingUntilTape, AligningWithBeacon, RotateCW45,
  RotateCW90, RotateCCW45, RotateCCW90
}MotorState_t;

// Public Function Prototypes

bool InitMotorFSM(uint8_t Priority);
bool PostMotorFSM(ES_Event_t ThisEvent);
ES_Event_t RunMotorFSM(ES_Event_t ThisEvent);
bool CheckForTape(void);

#endif /* MotorFSM_H */
