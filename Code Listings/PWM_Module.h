/****************************************************************************
PWM_Module Header File

 ****************************************************************************/

#ifndef PWM_Module_H
#define PWM_Module_H

#include "ES_Types.h"

//Public Protoypes
void PWM_Init(uint8_t NumPWM);
void PWM_SetFrequency(uint16_t PWMFreq, uint8_t PinNum);
void PWM_SetDuty(uint8_t PWMDuty, uint8_t PinNum);

#endif /*PWM_Module_H */
