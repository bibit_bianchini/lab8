/****************************************************************************
 Module
   SPIService.c

 Revision
   1.0.1

 Description
   This is the highest level module for ME 218B Lab 8.  Implements a flat
   state machine under the Gen2 Events and Services Framework.

 Notes
   Team 3

 History
 When           Who     What/Why
 -------------- ---     --------
 02/04/2019     pablo   created file
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "CommandFSM.h"
#include "SenseIR.h"
#include "MotorFSM.h"

#include "inc/hw_pwm.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_nvic.h"
#include "inc/hw_ssi.h"
#include "inc/hw_Timer.h"

/*----------------------------- Module Defines ----------------------------*/
#define BitsPerNibble 4

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match that of enum in header file

/****************************************************************************
 Function
     InitializeSPIService

 Parameters
     void

 Returns
     void

 Description
     Initializes SPI communications, and relevant hardware ports
 Notes

 Author
     Pablo Martinez Alanis
****************************************************************************/
void InitializeSPIService(void)
{
  // Activates clock to GPIO PortA
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R0;

  // Enables clock to SSI module 0
  HWREG(SYSCTL_RCGCSSI) |= SYSCTL_RCGCSSI_R0;

  // Waits for GPIO port to be ready
  while ((HWREG(SYSCTL_PRGPIO) & BIT0HI) != BIT0HI)
  {}

  // Programs GPIO for alternate functions
  HWREG(GPIO_PORTA_BASE + GPIO_O_AFSEL) |= (BIT2HI | BIT3HI | BIT4HI | BIT5HI);

  // PA2=SSIOClk, PA3=SSIOFss, PA4=SSIORx, PA5 SSIOTx
  // Writes in GPIOCTL to select desired functions
  HWREG(GPIO_PORTA_BASE + GPIO_O_PCTL) =
      (HWREG(GPIO_PORTA_BASE + GPIO_O_PCTL) & 0xff0000ff) + (2 << BitsPerNibble * 2)
      + (2 << BitsPerNibble * 3) + (2 << BitsPerNibble * 4) + (2 << BitsPerNibble * 5);

  // Programs PA2 to PA5 for digital I/O
  HWREG(GPIO_PORTA_BASE + GPIO_O_DEN) |= (BIT2HI | BIT3HI | BIT4HI | BIT5HI);

  // Programs PA2 to PA4 data directions
  // (PA2, PA3 and PA5 are outputs, PA4 is input)
  HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) |= (BIT2HI | BIT3HI | BIT5HI);
  HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) &= (BIT4LO);

  // Programs Pull-up for the clock line for SPI mode 3
  HWREG(GPIO_PORTA_BASE + GPIO_O_PUR) |= (BIT2HI);

  //Waits for SSI0 to be ready
  while ((HWREG(SYSCTL_RCGCSSI) & SYSCTL_RCGCSSI_R0) != SYSCTL_RCGCSSI_R0)
  {
    ;
  }
  {}
  printf("SPI Clock ready");

  // Disables SSI0 0
  HWREG(SSI0_BASE + SSI_O_CR1) &= (~SSI_CR1_SSE);

  // Select Master mode, and TXRIS for End of Transmission
  HWREG(SSI0_BASE + SSI_O_CR1) &= (~SSI_CR1_MS);
  HWREG(SSI0_BASE + SSI_O_CR1) |= (SSI_CR1_EOT);

  // Configures SSI clock source to be the system clock
  HWREG(SSI0_BASE + SSI_O_CC) = (SSI_CC_CS_SYSPLL);

  // Configures clock prescaler (Bit Rate 800 kHz)
  HWREG(SSI0_BASE + SSI_O_CPSR) = HWREG(SSI0_BASE + SSI_O_CPSR) & (0xFFFFFF00) + 50;

  // Configure SPH = 1 because data is captured at the second edge
  HWREG(SSI0_BASE + SSI_O_CR0) |= SSI_CR0_SPH;

  // Configure SPO = 1 because idle state is high
  HWREG(SSI0_BASE + SSI_O_CR0) |= SSI_CR0_SPO;

  // Freescale SPI Mode
  HWREG(SSI0_BASE + SSI_O_CR0) = ((HWREG(SSI0_BASE + SSI_O_CR0) &
      (~SSI_CR0_FRF_M)) | SSI_CR0_FRF_MOTO);

  // Sending 8 bit data between command generator & TIVA
  HWREG(SSI0_BASE + SSI_O_CR0) = ((HWREG(SSI0_BASE + SSI_O_CR0) &
      (~SSI_CR0_DSS_M)) | SSI_CR0_DSS_8);

  // Locally enable interrupts (TXIM in SSIIM)
  HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;

  // Enable SSI
  HWREG(SSI0_BASE + SSI_O_CR1) |= (SSI_CR1_SSE);

  // Enable NVIC bit 7 in EN0
  HWREG(NVIC_EN0) |= BIT7HI;

  // Globally enable interrupts
  //__enable_irq(); // commented out because this is done in CommandFSM init

  printf("SPI Initialized");
}

/****************************************************************************
 Function
     TransmitSPI

 Parameters
     uint8_t SendData data to send to the Command module

 Returns
     void

 Description
     Writes to the communication value the data to write
 Notes

 Author
     Pablo Martinez Alanis
****************************************************************************/
void TransmitSPI(uint8_t SendData)
{
  // Activates Local Interrupt
  HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;

  // Writes SendData to SSI Data Register
  HWREG(SSI0_BASE + SSI_O_DR) = SendData;

  // Debbuging aids
  //printf("DataSent");
  //printf("IE");
}

/****************************************************************************
 Function
    ReceiveSPI

 Parameters
   void

 Returns
   void

 Description
   Interrupt Service Routine for receiving interrupt after EOT

 Notes

 Author
   Pablo Martinez Alanis
****************************************************************************/
void ReceiveSPI(void)
{
  // Disables Local interrupts
  HWREG(SSI0_BASE + SSI_O_IM) &= (~SSI_IM_TXIM);

  // Sends Event to CommandFSM
  ES_Event_t PostEvent;
  PostEvent.EventType = SPI_RECEIVED;
  PostEvent.EventParam = HWREG(SSI0_BASE + SSI_O_DR);
  PostCommandFSM(PostEvent);

  //printf("Recieved SPI: %d \n\r",LastSPICommand);
}

/***************************************************************************
 private functions
 ***************************************************************************/
