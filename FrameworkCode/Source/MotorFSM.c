/****************************************************************************
 Module
   MotorFSM.c

 Revision
   1.0.1

 Description
   This is the motor module for ME 218B Lab 8.  Implements a flat
   state machine under the Gen2 Events and Services Framework.

 Notes

 History
 When           Who          What/Why
 -------------- ---          --------
 02/04/2019     bibit/robbie converted from templateFSM to lab 8 module
 01/15/12 11:12 jec          revisions for Gen2 framework
 11/07/11 11:26 jec          made the queue static
 10/30/11 17:59 jec          fixed references to CurrentEvent in RunTemplateSM
 10/23/11 18:20 jec          began conversion from SMTemplate.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "MotorFSM.h"
#include "CommandFSM.h"
#include "PWM_Module.h"

#include "inc/hw_pwm.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_nvic.h"
#include "inc/hw_Timer.h" \

#include <stdio.h>
#include <stdlib.h>

/*----------------------------- Module Defines ----------------------------*/
// timer names
#define MotorTimer 1
#define TakingTooLongTimer 2

// timing definitions
#define TooMuchTime 6000
#define TimeForTurn90 3600    // TODO: to be determined empirically
#define TimeForTurn45 1800    // TODO: to be determined empirically
#define MotorAOffset 3        // Decreases relative duty cycles on Motor A
#define MotorBOffset 0        // Decreases relative duty cycles on Motor B
#define CheckTapeTime 100
#define CheckIRTime 100

// PWM definitions
#define PWMFrequency 2000
#define HalfDuty 50
#define FullDuty 100
#define TurnDuty 50

// command byte definitions
#define STOP 0x00
#define ROTATECW90 0x02
#define ROTATECW45 0x03
#define ROTATECCW90 0x04
#define ROTATECCW45 0x05
#define HALFFORWARD 0x08
#define FULLFORWARD 0x09
#define HALFREVERSE 0x10
#define FULLREVERSE 0x11
#define ALIGNWITHBEACON 0x20
#define DRIVEUNTILTAPE 0x40

// set enable pins to be PB2 and PB3 for Motors A and B respectively
#define MotorAEnableLo BIT2LO
#define MotorAEnableHi BIT2HI
#define MotorBEnableLo BIT3LO
#define MotorBEnableHi BIT3HI

// set direction pins to be PB4 and PB5 for Motors A and B respectively
#define MotorADirectionLo BIT4LO
#define MotorADirectionHi BIT4HI
#define MotorBDirectionLo BIT5LO
#define MotorBDirectionHi BIT5HI

// pin definitions
#define TAPE_LINE_HI BIT1HI

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/
static void StopMotors(void);
static void DriveCW(void);
static void DriveCCW(void);
static void DriveForward(uint8_t Duty);
static void DriveReverse(uint8_t Duty);

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static MotorState_t CurrentState;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;

/*------------------------------ Module Code ------------------------------*/

/****************************************************************************
 Function
     InitMotorFSM

 Parameters
     uint8_t : the priority of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine

 Notes

 Author
     -
****************************************************************************/
bool InitMotorFSM(uint8_t Priority)
{
  ES_Event_t ThisEvent;
  MyPriority = Priority;
  // put us into the InitializeMotor state
  CurrentState = InitializeMotor;

  // Port B and E should have been initialized in Command Module

  // Call Init PWM function for 2 pins (PB6 + PB7)
  PWM_Init(2);

  // set PWM frequency on both channels to avoid torque ripple
  PWM_SetFrequency(PWMFrequency, 0);

  // Set PB2-PB5 as digital pins (for motor control)
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= (BIT2HI | BIT3HI | BIT4HI | BIT5HI);
  // set PB2-PB5 as outputs (for motor control)
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= (BIT2HI | BIT3HI | BIT4HI | BIT5HI);

  // set PB0 + PB1 as digital pins (for IR sensor + tape sensor)
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= (BIT0HI | BIT1HI);
  // set PB0 + PB1 as inputs (for IR sensor + tape sensor)
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) &= (BIT0LO & BIT1LO);

  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostMotorFSM

 Parameters
     ES_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostMotorFSM(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunMotorFSM

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   Receives command from CommandFSM service and performs the function.  Tells
   CommandFSM service when it is done and ready for the next command.

 Notes
   uses nested switch/case to implement the machine.

 Author
   Bibit Bianchini, 2/04/2019
****************************************************************************/
ES_Event_t RunMotorFSM(ES_Event_t ThisEvent)
{
  ES_Event_t  ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (CurrentState)
  {
    case InitializeMotor:        // If current state is InitializeMotor
    {
      // only respond to ES_Init
      if (ThisEvent.EventType == ES_INIT)
      {
        // begin with the motors stopped
        StopMotors();

        // put the machine into the actual initial state
        CurrentState = Idle;
      }
    }
    break;

    case Idle:        // If current state is Idle
    {
      // if we received a motor command
      if (ThisEvent.EventType == MOTOR_COMMAND)
      {
        printf("%x\n\r", ThisEvent.EventParam);
        // based on what command we receive, we should take different actions
        switch (ThisEvent.EventParam)
        {
          case STOP:
          {
            // stop motor
            StopMotors();
            // no need to change state since currently in Idle
            ES_Event_t PostEvent;
            PostEvent.EventType = MOTOR_READY;
            PostCommandFSM(PostEvent);
          }
          break;

          case ROTATECCW90:
          {
            // start turning counter-clockwise
            DriveCCW();

            // start MotorTimer for 90 degree rotation
            ES_Timer_InitTimer(MotorTimer, TimeForTurn90);

            // put in RotateCCW90 state
            CurrentState = RotateCCW90;
          }
          break;

          case ROTATECW90:
          {
            // start turning clockwise
            DriveCW();

            // start MotorTimer for 90 degree rotation
            ES_Timer_InitTimer(MotorTimer, TimeForTurn90);

            // put in RotateCW90 state
            CurrentState = RotateCW90;
          }
          break;

          case ROTATECCW45:
          {
            // start turning counter-clockwise
            DriveCCW();

            // start MotorTimer for 45 degree rotation
            ES_Timer_InitTimer(MotorTimer, TimeForTurn45);

            // put in RotateCCW45 state
            CurrentState = RotateCCW45;
          }
          break;

          case ROTATECW45:
          {
            // start turning clockwise
            DriveCW();

            // start MotorTimer for 45 degree rotation
            ES_Timer_InitTimer(MotorTimer, TimeForTurn45);

            // put in RotateCW90 state
            CurrentState = RotateCW45;
          }
          break;

          case FULLFORWARD:
          {
            // start driving full forward with full duty cycle
            DriveForward(FullDuty);

            // go to Idle state
            CurrentState = Idle;

            // post MOTOR_READY event to CommandFSM service
            ES_Event_t PostEvent;
            PostEvent.EventType = MOTOR_READY;
            PostCommandFSM(PostEvent);
          }
          break;

          case HALFFORWARD:
          {
            // start driving half forward with half duty cycle
            DriveForward(HalfDuty);

            // go to Idle state
            CurrentState = Idle;

            // post MOTOR_READY event to CommandFSM service
            ES_Event_t PostEvent;
            PostEvent.EventType = MOTOR_READY;
            PostCommandFSM(PostEvent);
          }
          break;

          case FULLREVERSE:
          {
            // start driving full reverse with full duty cycle
            DriveReverse(FullDuty);

            // go to Idle state
            CurrentState = Idle;

            // post MOTOR_READY event to CommandFSM service
            ES_Event_t PostEvent;
            PostEvent.EventType = MOTOR_READY;
            PostCommandFSM(PostEvent);
          }
          break;

          case HALFREVERSE:
          {
            // start driving half reverse with half duty cycle
            DriveReverse(HalfDuty);

            // go to Idle state
            CurrentState = Idle;

            // post MOTOR_READY event to CommandFSM service
            ES_Event_t PostEvent;
            PostEvent.EventType = MOTOR_READY;
            PostCommandFSM(PostEvent);
          }
          break;

          case ALIGNWITHBEACON:
          {
            // start TakeTooLongTimer for TooMuchTime in case our robot never
            // sees beacon
            ES_Timer_InitTimer(TakingTooLongTimer, TooMuchTime);

            // start robot turning
            DriveCW();

            // we want to be able to detect the IR rising edges, so enable
            // the input capture interrupt
            HWREG(NVIC_EN2) |= BIT30HI;

            // put in AligningWithBeacon state
            CurrentState = AligningWithBeacon;
          }
          break;

          case DRIVEUNTILTAPE:
          {
            // start TakeTooLongTimer for TooMuchTime in case our robot never
            // sees beacon
            ES_Timer_InitTimer(TakingTooLongTimer, TooMuchTime);

            // send robot forward at HalfDuty (can be changed)
            DriveForward(HalfDuty);

            // put in DrivingUntilTape state
            CurrentState = DrivingUntilTape;
          }
          break;
        }
      }  // end switch on CurrentEvent
    }
    break;

    case DrivingUntilTape:
    {
      switch (ThisEvent.EventType)
      {
        case TAPE_DETECTED:
        {
          // stop motors
          StopMotors();

          // post MOTOR_READY to CommandFSM service
          ES_Event_t PostEvent;
          PostEvent.EventType = MOTOR_READY;
          PostCommandFSM(PostEvent);

          // go into Idle state
          CurrentState = Idle;
        }
        break;

        case ES_TIMEOUT:
        {
          if (ThisEvent.EventParam == TakingTooLongTimer)
          {
            // stop motors
            StopMotors();

            // post MOTOR_READY event to CommandFSM service
            ES_Event_t PostEvent;
            PostEvent.EventType = MOTOR_READY;
            PostCommandFSM(PostEvent);

            // go to Idle state
            CurrentState = Idle;
          }
        }
        break;
      }
    }
    break;

    case AligningWithBeacon:
    {
      switch (ThisEvent.EventType)
      {
        // if we detected the beacon
        case ALIGNED_WITH_BEACON:
        {
          // the input capture interrupt is automatically disabled in the
          // ISR when it finds a good period.

          // stop moving motors
          StopMotors();

          // post MOTOR_READY event to CommandFSM service
          ES_Event_t PostEvent;
          PostEvent.EventType = MOTOR_READY;
          PostCommandFSM(PostEvent);

          // go to Idle state
          CurrentState = Idle;
        }

        // if the TakingTooLongTimer timed out
        case ES_TIMEOUT:
        {
          if (ThisEvent.EventParam == TakingTooLongTimer)
          {
            // stop motors
            StopMotors();

            // post MOTOR_READY event to CommandFSM service
            ES_Event_t PostEvent;
            PostEvent.EventType = MOTOR_READY;
            PostCommandFSM(PostEvent);

            // go to Idle state
            CurrentState = Idle;
          }
        }
      }
    }
    break;

    case RotateCW45:
    {
      // if the MotorTimer timed out
      if ((ThisEvent.EventType == ES_TIMEOUT) &
          (ThisEvent.EventParam == MotorTimer))
      {
        // stop the motors
        StopMotors();

        // post MOTOR_READY event to CommandFSM service
        ES_Event_t PostEvent;
        PostEvent.EventType = MOTOR_READY;
        PostCommandFSM(PostEvent);

        // set the state to Idle
        CurrentState = Idle;
      }
    }
    break;

    case RotateCW90:
    {
      // if the MotorTimer timed out
      if ((ThisEvent.EventType == ES_TIMEOUT) &
          (ThisEvent.EventParam == MotorTimer))
      {
        // stop the motors
        StopMotors();

        // post MOTOR_READY event to CommandFSM service
        ES_Event_t PostEvent;
        PostEvent.EventType = MOTOR_READY;
        PostCommandFSM(PostEvent);

        // set the state to Idle
        CurrentState = Idle;
      }
    }
    break;

    case RotateCCW45:
    {
      // if the MotorTimer timed out
      if ((ThisEvent.EventType == ES_TIMEOUT) &
          (ThisEvent.EventParam == MotorTimer))
      {
        // stop the motors
        StopMotors();

        // post MOTOR_READY event to CommandFSM service
        ES_Event_t PostEvent;
        PostEvent.EventType = MOTOR_READY;
        PostCommandFSM(PostEvent);

        // set the state to Idle
        CurrentState = Idle;
      }
    }
    break;

    case RotateCCW90:
    {
      // if the MotorTimer timed out
      if ((ThisEvent.EventType == ES_TIMEOUT) &
          (ThisEvent.EventParam == MotorTimer))
      {
        // stop the motors
        StopMotors();

        // post MOTOR_READY event to CommandFSM service
        ES_Event_t PostEvent;
        PostEvent.EventType = MOTOR_READY;
        PostCommandFSM(PostEvent);

        // set the state to Idle
        CurrentState = Idle;
      }
    }
    break;
  }                                   // end switch on Current State
  return ReturnEvent;
}

/****************************************************************************
 Function
     CheckForTape

 Parameters
     None

 Returns
     bool (true if tape detected, false otherwise)

 Description
     Event checker that reads the tape sensor pin and returns true if tape
     detected (pin high)

 Notes

 Author
     Bibit Bianchini, 2/04/2019
****************************************************************************/
bool CheckForTape(void)
{
  // Set CurrentTapeState to state read from port pin
  uint8_t CurrentTapeState = HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS));

  // if the CurrentTapeState is high:
  if ((CurrentTapeState & TAPE_LINE_HI) != 0)
  {
    // event has been detected, so post TAPE_DETECTED event to MotorFSM
    ES_Event_t PostEvent;
    PostEvent.EventType = TAPE_DETECTED;
    PostMotorFSM(PostEvent);

    // return true
    return true;
  }
  // otherwise, no tape detected, so return false
  else
  {
    return false;
  }
}

/***************************************************************************
 private functions
 ***************************************************************************/

/****************************************************************************
 Function
     StopMotors

 Parameters
     None

 Returns
     None

 Description
     Stops the motors by disabling them

 Notes

 Author
     Bibit Bianchini, 2/04/2019
****************************************************************************/
static void StopMotors(void)
{
  // set both enable pins low
  // (keep in mind other pins are still set)
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &=
      (MotorAEnableLo & MotorBEnableLo);
}

/****************************************************************************
 Function
     DriveCW

 Parameters
     None

 Returns
     None

 Description
     Drives the motors clockwise

 Notes

 Author
     Bibit Bianchini, 2/04/2019
****************************************************************************/
static void DriveCW(void)
{
  // enable both motors
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |=
      (MotorAEnableHi | MotorBEnableHi);

  // MotorA reverse, MotorB forward
  // --> set MotorB (PB5) direction to LO, Set MotorA(PB4) direction HI
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= MotorBDirectionLo;
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= MotorADirectionHi;

  // --> set MOTOR B Duty to TurnDuty
  PWM_SetDuty(TurnDuty - MotorBOffset,          1);
  // --> set MOTOR A duty to 100-TurnDuty
  PWM_SetDuty(100 - (TurnDuty - MotorAOffset),  0);
}

/****************************************************************************
 Function
     DriveCCW

 Parameters
     None

 Returns
     None

 Description
     Drives the motors counter-clockwise

 Notes

 Author
     Bibit Bianchini, 2/04/2019
****************************************************************************/
static void DriveCCW(void)
{
  // enable both motors
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |=
      (MotorAEnableHi | MotorBEnableHi);

  // MotorA forward, MotorB reverse
  // --> set MotorB (PB5) direction to HI, Set MotorA(PB4) direction LO
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= MotorBDirectionHi;
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= MotorADirectionLo;

  // --> set MOTOR A Duty to TurnDuty
  PWM_SetDuty((TurnDuty - MotorAOffset),        0);
  // --> set MOTOR B duty to 100-TurnDuty
  PWM_SetDuty(100 - (TurnDuty - MotorBOffset),  1);
}

/****************************************************************************
 Function
     DriveForward

 Parameters
     uint8_t (DutyCycle)

 Returns
     None

 Description
     Drives the motors forward at specified DutyCycle

 Notes

 Author
     Bibit Bianchini, 2/04/2019
****************************************************************************/
static void DriveForward(uint8_t Duty)
{
  // enable both motors
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |=
      (MotorAEnableHi | MotorBEnableHi);

  // MotorA and MotorB forward
  // --> set MotorDirection pins (PB4/PB5) low
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &=
      (MotorADirectionLo & MotorBDirectionLo);
  // --> set both motor's duties to given duty
  PWM_SetDuty((Duty - MotorAOffset),  0);
  PWM_SetDuty((Duty - MotorBOffset),  1);
}

/****************************************************************************
 Function
     DriveReverse

 Parameters
     uint8_t (DutyCycle)

 Returns
     None

 Description
     Drives the motors backwards at specified DutyCycle

 Notes

 Author
     Bibit Bianchini, 2/04/2019
****************************************************************************/
static void DriveReverse(uint8_t Duty)
{
  // enable both motors
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |=
      (MotorAEnableHi | MotorBEnableHi);

  // MotorA and MotorB reverse
  // --> set MotorDirection pins (PB4/PB5) HI
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |=
      (MotorADirectionHi | MotorBDirectionHi);

  // --> set both motor's duties to 100 - given duty
  PWM_SetDuty(100 - (Duty - MotorAOffset),  0);
  PWM_SetDuty(100 - (Duty - MotorBOffset),  1);
}
