/****************************************************************************

  Header file for Command Flat Sate Machine for ME 218B Lab 8 for
  Team 3, based on the Gen2 Events and Services Framework

 ***************************************************************************/

#ifndef CommandFSM_H
#define CommandFSM_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum { InitializeCommand, Check4Command, RunningCommand }
CommandState_t;

// Public Function Prototypes

bool InitializeCommandFSM(uint8_t Priority);
bool PostCommandFSM(ES_Event_t ThisEvent);
ES_Event_t RunCommandFSM(ES_Event_t ThisEvent);
void ReceiveSPI(void);

#endif /* CommandFSM_H */
