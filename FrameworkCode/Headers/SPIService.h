/****************************************************************************

  Header file for SPI module for ME218B Lab 8 Team 3

 ****************************************************************************/

#ifndef SPIService_H
#define SPIService_H

#include "ES_Framework.h"

// Public Function Prototypes

void InitializeSPIService(void);
void TransmitSPI(uint8_t SendData);
void ReceiveSPI(void);

#endif /* SPIService_H */
